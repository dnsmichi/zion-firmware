/*
 * minilibc
 *
 * (C) 2017 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of minilibc firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nome-Programma.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "string.h"
#include "stdlib.h"
#include "stdio.h"

#ifdef __GNUC__
#define va_arg(list,type) __builtin_va_arg(list,type)
#define va_start(ap,v)  __builtin_va_start(ap,v)
#endif

#define MAX_OPT_STRING  256

static char pbuff[2048];
static char ptemp[MAX_OPT_STRING];

std_output out_fn;

static char * itohex(int val, unsigned char caps)
{
	char *p = ptemp + (MAX_OPT_STRING - 1);
	unsigned char v;
	unsigned int uval = (unsigned int)val;

	*p-- = 0;
	do {
		v = uval % 16;
		if (v > 9)
			v += (caps) ? 55 : 87;
		else
			v += 0x30;

		*p-- = v;
		uval >>= 4;
	} while(uval);

	return ++p;
}

/*
 * read untils space or 0
 */
int atoi(char *s)
{
	int rval = 0;

	while (*s && ((*s >= 0x30) && (*s <= 0x39))) {
		rval *= 10;
		rval += (*s++ - 48);
	}

	return rval;
}

char * itoa(int val)
{
	char *p = ptemp + (MAX_OPT_STRING - 1);
	char sign_m = 0;

	*p-- = 0;

	if (val < 0)
		sign_m++;

	do {
		*p-- = (abs(val % 10) + '0'); val /= 10;
	} while(val);

	if (sign_m)
		*p-- = '-';

	return ++p;
}

/*
 * there must be array allocated backward (previous ptemp usage assumed)
 */
char * zpad(char *sz, int times)
{
	times = times - strlen(sz);
	while (times--) {*--sz = '0';}

	return sz;
}

/*
 * there must be array allocated backward (previous ptemp usage assumed)
 */
static char * pad(char *sz, int times)
{
	times = times - strlen(sz);

	while (times--) {*--sz = ' ';}

	return sz;
}

static char * get_opts(va_list *vl, char **opts)
{
	unsigned char padz = 0;
	unsigned char padc = 0;
	unsigned char padlj = 0;
	unsigned char opt;

	char *rval = 0;

	for (;;) {
		opt = **opts;

		switch(opt) {
		case '0':
			padz++;
		break;
		case '-':
			/* still no pad set ? */
			if (padc == 0) {
				/* so it ask for left justify */
				padlj = 1;
			}
		break;
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			padc *= 10;
			padc += (opt - 0x30);
		break;
		case 'X':
			rval = itohex(va_arg(*vl, int), 1);
		break;
		case 'x':
			rval = itohex(va_arg(*vl, int), 0);
		break;
		case 'd':
			rval = itoa(va_arg(*vl, int));
		break;
		case 's':
			rval = va_arg(*vl, char*);
		break;
		case '%':
			rval = "%";
		break;
		default:
			// end of possible specifiers, apply format
			if (padc) {
				if (!padlj) {
					rval = (padz) ? zpad(rval, padc) :
						pad(rval, padc);
				} else {
					int c, i, n, q;

					q = strlen(rval);
					n = padc - q;

					strcpy(ptemp, rval);

					for (c = 0, i = q; c < n; ++c, ++i)
						ptemp[i] = ' ';

					ptemp[i] = 0;

					rval = ptemp;
				}
			}
			// end exit
			return rval;
		}
		(*opts)++;
	}
	return 0;
}

/*
 * NOTE: on 64 bit passing a reference to va_list is no more
 * accepted
 */
int printf_seq(char* s, va_list vl)
{
	char *ps = s;
	char *pp = pbuff;
	char *conv;

	while (*ps) {
		if (*ps == '%') {
			++ps;
			if ((conv = get_opts(&vl, &ps))) {
				strcpy(pp, conv);
				pp += strlen(conv);
			} else
				goto exit;
		} else
			*pp++ = *ps++;
	}
	/* terminated */
	*pp = 0;
	out_fn((char*)pbuff);
exit:
	return strlen(pbuff);
}

int printf(char* s, ...)
{
	va_list vl;
	va_start(vl, s);

	return printf_seq(s, vl);
}

void set_std_output(std_output func)
{
	out_fn = func;
}

