/*
 * minilibc
 *
 * (C) 2017 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of minilibc firmware library.
 *
 * minilibc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nome-Programma.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "string.h"

void *memset(void *ptr, int value, size_t num)
{
	char *p = (void *)ptr;

	if (!p)
		return ptr;

	while (num--)
		*p++ = value;

	return ptr;
}

void *memcpy(void *dest, const void *src, size_t num)
{
	char *d = (void *)dest;
	char *s = (void *)src;

	if (!dest || !src)
		return dest;

	while (num--)
		*d++ = *s++;

	return dest;
}

size_t strlen(const char *s)
{
	size_t len = 0;

	while (*s++ != 0)
		len++;

	return len;
}

char *strcpy(char *dest, const char *src)
{
	char *rval = dest;

	if (!dest || !src)
		return rval;

	while(*src != 0)
		*dest++ = *src++;

	*dest = 0;

	return rval;
}

char *strncpy(char *dest, const char *src, size_t n)
{
	char *rval = dest;

	if (!dest || !src || !n)
			return rval;

	while(n--) {
		if (*src == 0)
			break;
		*dest++ = *src++;
	}

	*dest = 0;

	return rval;
}
