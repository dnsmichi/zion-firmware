Zion Firmware - a compact FOSS cortex-m4 firmware framework

# SPDX-License-Identifier: GPL-2.0+
#
# (C) Copyright 2020
# Angelo Dureghello <angelo.dureghello@timesys.com>.

1. Introduction

This projects is a full stm32 cortex-m4 development framework, written
totally from scratch, from start.s and linker scripts, without reusing
any ST library part.

Main purpose was to enjoy, have a final binary as small as possible, so
all makefiles are strongly tuned for this. 

The framework is composed by:

- a small libc (libc folder),
- a driver library, startup files included (lib),
- a set of demo bare-metal firmware projects, as those in thermo or test,
  linking against lib and libc.


2. Build

Export the cortex-m4 toolchian path:
export CROSS_COMPILE=/opt/toolchains/arm/gcc-arm-8.3-2019.03-....

Then for each folder (libc, lib, firmware) just:

make clean
make

3. Program the cortex-m4 internal flash

Programming is now supported, using st-link v2 right now. 

To program, use:

make burn

from the firmware folder.

Thanks for using (or considering using) it.
Please provide any feedback to angelo70@gmail.com
