/*
 * zion
 *
 * (C) 2020 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of zion firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zion.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "init.h"

#include <clock.h>
#include <time.h>
#include <gpio.h>
#include <console.h>
#include <stdio.h>
#include <dht11.h>

extern struct systime_t tm;
static const int correction = 1;

static void uptime(void)
{
	console_set_cursor(0, 6);

	printf("%4dgg %02d:%02d:%02d",
		tm.day,
		tm.hour,
		tm.min,
		tm.sec);
}

static void alive(void)
{
	static int status = 0;

	status ^= 1;
	gpio_set_value(port_a, 12, status);
}

int main()
{
	uint32_t secs, secs_read = 0, old_secs = 0;
	struct th th;

	sys_init();

	console_clear();

	printf("T/H monitor\nv.091(alpha)\n\n");
	printf("T:        *C\n");
	printf("H:        %%\n");

	for (;;) {
		secs = get_sys_secs();

		if (old_secs < secs) {
			old_secs = secs;
			alive();
			uptime();
		}

		if ((secs - secs_read) > 10) {
			secs_read = secs;
			if (dht11_read(&th) == 0) {
				console_set_cursor(3, 3);
				printf("       ");
				console_set_cursor(3, 3);
				th.ti -= correction;
				printf("%d.%d", th.ti, th.td);
				console_set_cursor(3, 4);
				printf("       ");
				console_set_cursor(3, 4);
				printf("%d.%d", th.hi, th.hd);
			} else {
				console_set_cursor(3, 3);
				printf("---.---");
				console_set_cursor(3, 4);
				printf("---.---");
			}
		}
	}

	power_enter_stop();

	return 0;
}
