/*
 * libzion
 *
 * (C) 2020 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of libzion firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libzion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libzion.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <clock.h>
#include <arch.h>
#include <i2c.h>

static volatile struct i2c_regs *i2c;

static inline uint8_t nack(void)
{
	return i2c->isr & ISR_NACKF;
}

static inline void start(void)
{
	i2c->cr2 |= CR2_START;
}

static inline void stop()
{
	i2c->cr2 |= CR2_STOP;
}

static inline void wait_txdr_free()
{
	while (!(i2c->isr & ISR_TXIS))
		;
}

static inline void wait_bus_free(void)
{
	while (i2c->isr & ISR_BUSY)
		;
}

static inline void wait_completed(void)
{
	while (!(i2c->isr & ISR_TC))
		;
}

void i2c_write(uint8_t addr, uint8_t *buff, uint8_t len)
{
	/* Clean and set */
	i2c->cr2 = ((addr << 1) & 0x3ff);
	i2c->cr2 &= ~CR2_RD_WR;
	i2c->cr2 |= (len << 16);

	wait_bus_free();
	start();

	while (len--) {
		if (nack())
			goto exit_loop;

		wait_txdr_free();
		i2c->txdr = *buff++;
	}

	wait_completed();

exit_loop:
	stop();
}

/*
 * Before enabling the peripheral, the I2C master clock must be configured
 * by setting the SCLH and SCLL bits in the I2C_TIMINGR register.
 *
 * HSI clock is the default source for i2c, clock speed (8MHZ).
 */
static void i2c_configure_clock(void)
{
	if (!i2c)
		return;

#ifdef CLOCK_SPEED_8MHZ
	/* Starting test, HSI 8Mhz 400Khz, as per ref. manual */
	i2c->timingr = TIMINGR_PRESC(0) |
			TIMINGR_SCLL(9) |
			TIMINGR_SCLH(3) |
			TIMINGR_SDADEL(1) |
			TIMINGR_SCLDEL(3);
	/* Scope-tested we are near 370Khz */
#endif
}

/*
 * Index starts from 0, so 0 or 1 (for now)
 *
 * On stm32f303k8, there is only I2C1, and chances are:
 * PA14/PA15 respectively SDA/SCL or,
 * PB6/PB7   respectively SCL/SDA or,
 * PB8/PB9   respectively SCL/SDA
 *
 * Interface get auto-set to master at START generated
 */
void init_i2c(int idx)
{
	if (idx < 0 || idx > 1)
		return;

	/* Periph needs to be enabled seaprately */
	i2c = (volatile struct i2c_regs *)get_base(hm_i2c1 + idx);

	i2c_configure_clock();

	i2c->cr1 = CR1_TXIE | CR1_RXIE | CR1_NACKIE | CR1_TCIE;
	i2c->cr1 |= CR1_PE;
}