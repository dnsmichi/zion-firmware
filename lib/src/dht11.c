/*
 * libzion
 *
 * (C) 2020 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of libzion firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libzion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libzion.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <dht11.h>
#include <time.h>

struct drv_data {
	enum port port;
	int pos;
};

static struct drv_data drv;

static int dht11_send_start_signal(void)
{
	gpio_set_direction(drv.port, drv.pos, dir_out);
	gpio_set_value(drv.port, drv.pos, 0);

	delay_ms(19);

	gpio_set_direction(drv.port, drv.pos, dir_in);

	delay_us(50);

	if (gpio_get_value(drv.port, drv.pos) != 0)
		return -1;

	/* sync */
	while (gpio_get_value(drv.port, drv.pos) == 0)
		;

	return 0;
}

static unsigned long dht11_rx_40bit_train(void)
{
	int i, prev_was_1 = 1;
	unsigned long data = 0;

	for (i = 0; i < 32; ++i) {
		if (prev_was_1) {
			/* wait initial transition to 0 */
			while (gpio_get_value(drv.port, drv.pos) == 1)
				;
		}
		/* Wait transition to 1, always */
		while (gpio_get_value(drv.port, drv.pos) == 0)
			;
		/* Ok, here bit starts */
		delay_us(32);
		data <<= 1;
		if (gpio_get_value(drv.port, drv.pos) == 1) {
			data |= 1;
			prev_was_1 = 1;
		} else {
			prev_was_1 = 0;
		}
	}

	/* let crc go */
	delay_ms(2);

	return data;
}

int dht11_read(struct th *th)
{
	unsigned long data;

	if (dht11_send_start_signal() != 0)
		return -1;

	data = dht11_rx_40bit_train();

	th->hi = (data >> 24);
	th->hd = (data >> 16) & 0xff;
	th->ti = (data >> 8) & 0xff;
	th->td = data & 0xff;

	/*
	 * We want be sure sensor is standby, let the bus to pull-up,
	 * as described in the ds.
	 */
	//gpio_set_direction(drv.port, drv.pos, dir_out);
	//gpio_set_value(drv.port, drv.pos, 1);

	return 0;
}

void init_dht11(enum port sensor_port, int pos)
{
	drv.port = sensor_port;
	drv.pos = pos;

	/* We are 3,3, cannot pull up, let the bus go up */
	gpio_set_direction(drv.port, drv.pos, dir_in);
	gpio_set_pull(drv.port, drv.pos, pull_up);
}
