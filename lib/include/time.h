#ifndef __time_h
#define __time_h

#include <stdint.h>

struct timer_ac {
	uint32_t cr1;
	uint32_t cr2;
	uint32_t smcr;
	uint32_t dier;
	uint32_t sr;
	uint32_t egr;
	uint32_t ccmr1;
	uint32_t ccmr2;
	uint32_t ccer;
	uint32_t cnt;
	uint32_t psc;
	uint32_t arr;
	uint32_t rcr;
	uint32_t ccr1;
	uint32_t ccr2;
	uint32_t ccr3;
	uint32_t ccr4;
	uint32_t bdtr;
	uint32_t dcr;
	uint32_t dmar;
	uint32_t or;
	uint32_t ccmr3;
	uint32_t ccr5;
	uint32_t ccr6;
};

#define CR1_CEN		(1 << 0)

struct systime_t {
	uint8_t sec;
	uint8_t min;
	uint8_t hour;
	uint32_t day;
};

void init_sys_timer(void);
void delay_ms(int ms);
void delay_us(int us);
uint32_t get_sys_secs(void);

#endif /* __time_h */