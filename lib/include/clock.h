#ifndef __clock_h
#define __clock_h

#include <stdint.h>

#ifndef CLOCK_SPEED
/* Internal HSI */
#define CLOCK_SPEED_8MHZ
#endif

enum periph {
	p_dma1,
	p_dma2,
	p_sram,
	p_flitf = 4,
	p_fmc,
	p_crc,
	p_ioph = 16,
	p_iopa,
	p_iopb,
	p_iopc,
	p_iopd,
	p_iope,
	p_iopf,
	p_iopg,
	p_tsc,
	p_adc12 = 28,
	p_adc34,
	p_syscfg = 32,
	p_tim1 = 43,
	p_spi1,
	p_tim8,
	p_usart1,
	p_spi4,
	p_tim15,
	p_tim16,
	p_tim17,
	p_tim2 = 64,
	p_tim3,
	p_tim4,
	p_usart2 = 81,
	p_usart3,
	p_uart4,
	p_uart5,
	p_i2c1,
	p_i2c2,
};

struct rcc {
	uint32_t cr;
	uint32_t cfgr;
	uint32_t cir;
	uint32_t apb2rstr;
	uint32_t apb1rstr;
	uint32_t ahbenr;
	uint32_t apb2enr;
	uint32_t apb1enr;
	uint32_t bdcr;
	uint32_t csr;
	uint32_t ahbrstr;
	uint32_t cfgr2;
	uint32_t cfgr3;
};

#define PWR_LPDS	(1 << 0)
#define PWR_PDDS	(1 << 1)

struct pwr {
	uint32_t cr;
	uint32_t csr;
};

void clock_enable(enum periph n);
void power_enter_sleep(void);
void power_enter_stop(void);

#endif /* __clock_h */