#ifndef __gpio_h
#define __gpio_h

#include <stdint.h>

struct gpio {
	uint32_t moder;
	uint32_t otyper;
	uint32_t ospeedr;
	uint32_t pupdr;
	uint32_t idr;
	uint32_t odr;
	uint32_t bsrr;
	uint32_t lckr;
	uint32_t afrl;
	uint32_t afrh;
	uint32_t brr;
};

enum port {
	port_a,
	port_b,
};

enum type {
	dir_out,
	dir_in,
};

enum out_type {
	push_pull,
	open_drain,
};

enum speed_type {
	low,
	medium,
	high,
};

enum pull {
	pull_up,
	pull_down,
	pull_disabled,
};

void gpio_set_direction(enum port p, int bit, enum type);
void gpio_set_value(enum port p, int bit, int value);
int gpio_get_value(enum port p, int bit);
void gpio_alternate_func(enum port p, int bit, int value);
void gpio_set_out_type(enum port p, int bit, enum out_type);
void gpio_set_speed(enum port p, int bit, enum speed_type speed);
void gpio_set_pull(enum port p, int bit, enum pull pull_type);

#endif /* __gpio_h */